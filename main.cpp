

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

char g=0;

ISR(TIMER0_OVF_vect) {
	g = ~g;
		if (g==0)
		PORTD |= (1<<5);
		if (g!=0)
	PORTD &= ~(1<<5);
	//	PORTD = 0xFF;
		TCNT0 = 0xDA;//40;
}

void Timer_Init() {
	TIMSK0=(1<<TOIE0);	/* Enable timer interrupt */
	TCNT0 = 0xFA;//40;		/* load TCNT0, count for 13us */
	TCCR0B = (1<<CS00);	/* Timer0, normal mode, /8 prescalar */

}


void ADC_init(char pin) {
  ADMUX = (1 << 6) | (1<<ADLAR) | (pin & 0b111); // ADC6, left adjusted
  ADCSRA = (1 << ADEN) | (6 << ADPS0); // 125kHz
  DDRC = 0;
  PORTC=0;
}

unsigned char ADC_read(char pin) {
  ADC_init(pin);
  //  ADMUX &= 0b11111 & (pin & 0b111);
  ADCSRA |= (1<<ADSC);
  while(!(ADCSRA & (1 << ADIF)));
  ADCSRA |= (1<<ADIF);                    // Remet ADIF a 0 en inscrivant 1 !
  return (ADCH);
}

void USART_Init(unsigned int ubrr){
  DDRD=0xFF;//(1<<PD1);
 /*Set baud rate */

  UBRR0H = (unsigned char)(ubrr>>8);
  UBRR0L = (unsigned char)ubrr;
  /* Enable receiver and transmitter */
  UCSR0B = (1<<TXEN0);
  /* Set frame format: 8data, 2stop bit */
  UCSR0C = (1<<USBS0)|(3<<UCSZ00);
  /*
  UBRR0H = (unsigned char)(ubrr>>8) ; // UBRRH = 0x00
  UBRR0L = (unsigned char)ubrr ;
  // UBRRL = 11 : UBRR = f(osc)/(16*BAUD)

  UCSR0C = (3<<UCSZ00);// USBS : bit STOP
  // UCSZ0 : nb de bit transmis
  // Aucune Parite: UPM1=0 et UPM = 0UCSR0B |= (1 << TXEN0); /*(1 << RXEN0) |*/



}

int USART_TxChar(char car)// fonction envoyer caractere
{
  while(!(UCSR0A&(1<<UDRE0)));// attendre fin d'emission du caractere precedent

  UDR0 = car;//envoyer caractere
  return 0;
}




unsigned char USART_RxChar(void) {// fonction attendre reception
  while(!(UCSR0A&(1<<RXC0))); // attendre la reception
  return UDR0;
}


unsigned char toServo(unsigned char val) {
  if (val > 180)
    val = 180;

  return val;
}


int main(void)
{
CLKPR = (1<<CLKPCE);
CLKPR = 0;

 DDRD |= (1 << 5);		/* set PORTD as output port */
	PORTD = 0xFF;
	USART_Init(420);	/* set baud rate 1200 */
	sei();			/* enable global interrupt */
	Timer_Init();		/* call timer function */
	//	ADC_init();

	while(1)
	{
	  unsigned char x = toServo(ADC_read(0))/2 + 30;
	  unsigned char y = toServo(ADC_read(1))/2 + 30;
	  unsigned char z = toServo(ADC_read(6));
	  //	  UCSR0B = (1<<TXEN0);
	  _delay_ms(20);
	  USART_TxChar('$');/* send the special character '$' */
	  //	  _delay_ms(15);
	  USART_TxChar(x);/* send '1' */
	  USART_TxChar(x);/* send '1' */
	  //_delay_ms(15);
	  USART_TxChar(y);/* send '1' */
	  USART_TxChar(y);/* send '1' */
	  //_delay_ms(15);
	  USART_TxChar(y);/* send '1' */
	  USART_TxChar(y);/* send '1' */
	  //_delay_ms(15);
	  USART_TxChar(z);/* send '1' */
	  USART_TxChar(z);/* send '1' */
	  //_delay_ms(15);
	  //USART_TxChar('2');/* send invert of '1' */
	  //_delay_ms(20);
	  //	  USART_TxChar('3');/* send invert of '1' */
	  //_delay_ms(20);

	  USART_TxChar('\n');/* send invert of '1' */
	  //_delay_ms(20);
	  //USART_TxChar('\0');/* send invert of '1' */
	  //_delay_ms(20);
	  //UCSR0B &= ~(1<<TXEN0);
	    //  PORTD = 0;
	  _delay_ms(25);
	}
}
