CC = avr-gcc
CFLAGS = -D __AVR_ATmega168__ -D F_CPU=16000000 -g3 -O1 -lavr-libc
DEPS = main.o
OUT = main.hex

all :  $(DEPS)
	$(CC) -mmcu=atmega168 $(CFLAGS) $(DEPS)  -o tmp.elf
	avr-objcopy -O ihex tmp.elf $(OUT)

send : all
	sudo avrdude -c usbasp -p t4313 -e -U flash:w:$(OUT)
